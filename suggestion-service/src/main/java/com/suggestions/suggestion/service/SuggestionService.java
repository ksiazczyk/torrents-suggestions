package com.suggestions.suggestion.service;

public interface SuggestionService {

	String findByName(String suggestionName);

}
