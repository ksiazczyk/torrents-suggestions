package com.suggestions.suggestion.controller;

import com.suggestions.suggestion.client.AccountsServiceClient;
import com.suggestions.suggestion.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SuggestionController {

	@Autowired
	private AccountsServiceClient accountsServiceClient;

	@RequestMapping(path = "/{profileId}", method = RequestMethod.GET)
	public String getSuggestionsForAccount(@PathVariable String profileId) {
		Account account = accountsServiceClient.getAccountByProfileId(profileId);
		return account.toString();
	}

}
