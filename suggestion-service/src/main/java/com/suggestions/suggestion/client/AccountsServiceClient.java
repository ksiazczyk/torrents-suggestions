package com.suggestions.suggestion.client;

import com.suggestions.suggestion.domain.Account;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "account-service")
public interface AccountsServiceClient {

    @RequestMapping(value = "/accounts/{profileId}", method = RequestMethod.GET)
    Account getAccountByProfileId(@PathVariable("profileId") String profileId);

}
