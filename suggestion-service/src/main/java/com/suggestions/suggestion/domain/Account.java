package com.suggestions.suggestion.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

	private String profileId;

	private Double minimalRating;

	private List<String> genresWhiteList;

	private List<String> genresBlackList;

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Double getMinimalRating() {
		return minimalRating;
	}

	public void setMinimalRating(Double minimalRating) {
		this.minimalRating = minimalRating;
	}

	public List<String> getGenresWhiteList() {
		return genresWhiteList;
	}

	public void setGenresWhiteList(List<String> genresWhiteList) {
		this.genresWhiteList = genresWhiteList;
	}

	public List<String> getGenresBlackList() {
		return genresBlackList;
	}

	public void setGenresBlackList(List<String> genresBlackList) {
		this.genresBlackList = genresBlackList;
	}

	@Override
	public String toString() {
		return "Account{" +
				"profileId='" + profileId + '\'' +
				", minimalRating=" + minimalRating +
				", genresWhiteList=" + genresWhiteList +
				", genresBlackList=" + genresBlackList +
				'}';
	}
}
