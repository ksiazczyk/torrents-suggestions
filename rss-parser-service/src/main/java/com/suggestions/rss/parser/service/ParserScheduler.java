package com.suggestions.rss.parser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by gksiazcz on 09.01.17.
 */
@Service
public class ParserScheduler {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    RssParserService rssParserService;

    @Scheduled(cron = "0 0/2 * * * ?")
//    @Scheduled(cron = "0 0 1/12 * * ?")
    public void readTorrentsNewsAndParse() {
        log.info(" scheduler fired -> " + System.currentTimeMillis());
        try {
            rssParserService.readTorrentsNews();
        } catch (IOException e) {
            log.error("Problem with parsing latest news", e);
        }
    }

}
