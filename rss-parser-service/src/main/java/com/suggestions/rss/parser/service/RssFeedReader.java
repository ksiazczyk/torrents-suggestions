package com.suggestions.rss.parser.service;

import com.rometools.rome.feed.synd.SyndEntry;

import java.io.IOException;
import java.util.List;

/**
 * Created by gksiazcz on 09.01.17.
 */
public interface RssFeedReader {

    List<SyndEntry> getSyndFeedEntries() throws IOException;
}
