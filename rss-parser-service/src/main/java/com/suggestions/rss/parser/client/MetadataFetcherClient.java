package com.suggestions.rss.parser.client;

import com.suggestions.rss.parser.domain.Torrent;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by gksiazcz on 10.01.17.
 */
@FeignClient(name = "metadata-fetcher-service")
public interface MetadataFetcherClient {

        @RequestMapping(method = RequestMethod.POST, value = "/metadata-fetcher/", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
        @ResponseBody
        Torrent updateOrCreateTorrent(@RequestBody Torrent torrent);

}
