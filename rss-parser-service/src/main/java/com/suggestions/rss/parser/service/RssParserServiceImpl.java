package com.suggestions.rss.parser.service;

import com.rometools.rome.feed.synd.SyndEntry;
import com.suggestions.rss.parser.client.MetadataFetcherClient;
import com.suggestions.rss.parser.domain.Torrent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RssParserServiceImpl implements RssParserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MetadataFetcherClient metadataFetcherClient;

	@Autowired
	private RssFeedReader rssFeedReader;

	@Override
	public void readTorrentsNews() {
		try {
			List<SyndEntry> entries = rssFeedReader.getSyndFeedEntries();

			if(entries != null) {
				List<Torrent> newTorrents = entries.stream()
						.map(syndEntryToTorrent)
						.collect(Collectors.<Torrent> toList());

				log.info("there is {} torrents in the feed", newTorrents.size());

				for (Torrent torrent : newTorrents) {
					metadataFetcherClient.updateOrCreateTorrent(torrent);
					log.info("{} -> {}",torrent.getTitle(), torrent.getUrl());
				}
			}
		} catch (IOException e) {
			log.error("error while trying to read rss feed", e);
		}
	}

	Function<SyndEntry, Torrent> syndEntryToTorrent = syndEntry -> {
        Torrent torrent = new Torrent();
        torrent.setTitle(syndEntry.getTitle());
        torrent.setUrl(syndEntry.getUri());
        torrent.setPublicationDate(syndEntry.getPublishedDate());
		torrent.setModificationDate(syndEntry.getUpdatedDate());
        return torrent;
    };


}
