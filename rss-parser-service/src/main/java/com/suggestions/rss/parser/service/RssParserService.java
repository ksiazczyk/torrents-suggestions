package com.suggestions.rss.parser.service;

import java.io.IOException;

public interface RssParserService {

    void readTorrentsNews() throws IOException;

}
