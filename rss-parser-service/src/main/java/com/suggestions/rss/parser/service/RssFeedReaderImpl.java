package com.suggestions.rss.parser.service;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Created by gksiazcz on 09.01.17.
 */
@Service
public class RssFeedReaderImpl implements RssFeedReader {

    public static final String GZIP = "gzip";
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${rssfeed.url}")
    String url;

    @Override
    public List<SyndEntry> getSyndFeedEntries() throws IOException {
        SyndFeed feed = null;
        InputStream is = null;

        try {
            URLConnection openConnection = new URL(url).openConnection();
            is = new URL(url).openConnection().getInputStream();
            if(GZIP.equals(openConnection.getContentEncoding())){
                is = new GZIPInputStream(is);
            }
            InputSource source = new InputSource(is);
            SyndFeedInput input = new SyndFeedInput();
            feed = input.build(source);

        } catch (Exception e){
            log.error("Exception occured when building the feed object out of the url", e);
        } finally {
            if( is != null)	is.close();
        }


        return feed != null ? feed.getEntries() : null;

    }
}
