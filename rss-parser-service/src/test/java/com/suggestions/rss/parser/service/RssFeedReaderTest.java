package com.suggestions.rss.parser.service;

import com.rometools.rome.feed.synd.SyndEntry;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.List;

import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by gksiazcz on 10.01.17.
 */
public class RssFeedReaderTest {

    @InjectMocks
    private RssFeedReaderImpl rssFeedReader;

    @Before
    public void setup() {
        initMocks(this);
        rssFeedReader.url = "file:" + this.getClass().getResource("/rss.xml").getPath();
    }

    @Test
    public void testGetSyndFeedEntries() throws Exception {
        List<SyndEntry> syndFeedEntries = rssFeedReader.getSyndFeedEntries();
        Assert.assertNotNull(syndFeedEntries);
        Assert.assertTrue(syndFeedEntries.size()>0);
    }
}