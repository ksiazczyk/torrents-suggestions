package com.suggestions.rss.parser.service;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.suggestions.rss.parser.client.MetadataFetcherClient;
import com.suggestions.rss.parser.domain.Torrent;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by gksiazcz on 10.01.17.
 */
public class RssParserServiceTest {

    @InjectMocks
    private RssParserServiceImpl rssParserService;

    @Mock
    private MetadataFetcherClient metadataFetcherClient;

    @Mock
    private RssFeedReader rssFeedReader;
    private List<SyndEntry> result;

    @Before
    public void setup() {
        initMocks(this);
        fillResultListWithSamples();
    }

    @Test
    public void testReadTorrentsNews() throws Exception {
        try {
            Mockito.when(rssFeedReader.getSyndFeedEntries()).thenReturn(result);
        } catch (IOException e) {
            Assert.assertFalse(true);
        }

        rssParserService.readTorrentsNews();
        verify(metadataFetcherClient, times(result.size())).updateOrCreateTorrent(any(Torrent.class));
    }

    @Test
    public void testReadTorrentNewsWithEmptyTorrentsList() {
        try {
            Mockito.when(rssFeedReader.getSyndFeedEntries()).thenReturn(new ArrayList<>());
        } catch (IOException e) {
            Assert.assertFalse(true);
        }

        rssParserService.readTorrentsNews();
        verify(metadataFetcherClient, times(0)).updateOrCreateTorrent(any(Torrent.class));
    }

    @Test
    public void testReadTorrentNewsWithNullTorrentsList() {
        try {
            Mockito.when(rssFeedReader.getSyndFeedEntries()).thenReturn(null);
        } catch (IOException e) {
            Assert.assertFalse(true);
        }

        rssParserService.readTorrentsNews();
        verify(metadataFetcherClient, times(0)).updateOrCreateTorrent(any(Torrent.class));
    }

    private void fillResultListWithSamples() {
        result = new ArrayList<>();
        result.add(getSyndEntryStub("title1", "uri 1"));
        result.add(getSyndEntryStub("title2", "uri 2"));
        result.add(getSyndEntryStub("title3", "uri 3"));
        result.add(getSyndEntryStub("title4", "uri 4"));
        result.add(getSyndEntryStub("title5", "uri 5"));
    }

    private SyndEntry getSyndEntryStub(String title, String uri) {
        SyndEntry entry1 = new SyndEntryImpl();
        entry1.setTitle(title);
        entry1.setUri(uri);
        return entry1;
    }

}