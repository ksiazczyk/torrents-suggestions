/**
 * Creates pre-filled demo account
 */

print('dump start');

db.accounts.update(
    { "_id": "demo" },
    {
    "_id": "demo",
    "minimalRating": 3.1,
    "genresWhiteList": ["Commedy", "Sci-Fi"],
    "genresBlackList": ["Drama", "Biography", "Horror", "Romance"]
    },
    { upsert: true }
);
db.accounts.update(
    { "_id": "demo2" },
    {
    "_id": "demo2",
    "minimalRating": 3.4,
    "genresWhiteList": ["Commedy"],
    "genresBlackList": ["Drama", "Horror", "Romance"]
    },
    { upsert: true }
);
db.accounts.update(
    { "_id": "demo3" },
    {
    "_id": "demo3",
    "minimalRating": 4.1,
    "genresWhiteList": ["Sci-Fi"],
    "genresBlackList": ["Horror", "Romance"]
    },
    { upsert: true }
);

print('dump complete');