#!/bin/bash

# docker tag local-image:tagname reponame:tagname
# docker push reponame:tagname

# docker login
# -e $DOCKER_EMAIL -u $DOCKER_USER -p $DOCKER_PASS

#push all to docker repo
docker login
docker push blackjedi/dockerepo:suggestions-config
docker push blackjedi/dockerepo:suggestions-registry
docker push blackjedi/dockerepo:suggestions-monitoring
docker push blackjedi/dockerepo:suggestions-gateway
docker push blackjedi/dockerepo:suggestions-mongodb

docker push blackjedi/dockerepo:suggestions-account-service
docker push blackjedi/dockerepo:suggestions-suggestion-service
docker push blackjedi/dockerepo:suggestions-rss-parser-service
docker push blackjedi/dockerepo:suggestions-metadata-fetcher-service


# ls -lah | xargs -P 100 -n 1 wget http://localhost/suggestions/demo