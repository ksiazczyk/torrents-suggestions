package com.suggestions.metadata.fetcher.service;

import com.suggestions.metadata.fetcher.domain.Torrent;
import com.suggestions.metadata.fetcher.repository.TorrentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetadataFetcherServiceImpl implements MetadataFetcherService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private TorrentRepository repository;

	@Override
	public Torrent fetchAndSave(Torrent torrent) {
		Torrent result = torrent;
		log.info("saved -> {}", torrent.getTitle());
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			log.error("something is wrong", e);
//		}
//		List<String> genres = new ArrayList<>();
//		genres.add("drama");
//		genres.add("comedy");
//		result.setGenres(genres);
//		result.setRating(4.3);
//		repository.save(result);
		return result;
	}
}
