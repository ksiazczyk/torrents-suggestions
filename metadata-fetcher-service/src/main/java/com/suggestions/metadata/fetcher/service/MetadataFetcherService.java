package com.suggestions.metadata.fetcher.service;

import com.suggestions.metadata.fetcher.domain.Torrent;

public interface MetadataFetcherService {

    Torrent fetchAndSave(Torrent torrent);

}
