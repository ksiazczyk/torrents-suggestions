package com.suggestions.metadata.fetcher.repository;

import com.suggestions.metadata.fetcher.domain.Torrent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TorrentRepository extends CrudRepository<Torrent, String> {

    Torrent findByTitle(String title);

}
