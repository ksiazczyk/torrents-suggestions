package com.suggestions.metadata.fetcher.controller;

import com.suggestions.metadata.fetcher.domain.Torrent;
import com.suggestions.metadata.fetcher.service.MetadataFetcherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MetadataFetcherController {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MetadataFetcherService metadataFetcherService;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody Torrent updateOrCreateTorrent(@RequestBody Torrent torrent){
		log.info(torrent.toString());
		metadataFetcherService.fetchAndSave(torrent);
		return torrent;
	}


}
