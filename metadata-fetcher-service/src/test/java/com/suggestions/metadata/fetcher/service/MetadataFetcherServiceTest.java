package com.suggestions.metadata.fetcher.service;

import com.suggestions.metadata.fetcher.domain.Torrent;
import com.suggestions.metadata.fetcher.repository.TorrentRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Date;

import static org.mockito.MockitoAnnotations.initMocks;

public class MetadataFetcherServiceTest {

	@InjectMocks
	private MetadataFetcherServiceImpl metadataFetcherService;

	@Mock
	private TorrentRepository repository;

	@Before
	public void setup() {
		initMocks(this);
	}


	@Test
	public void shouldSaveTorrentsData() {

		metadataFetcherService.fetchAndSave(getStubTorrent());

	}

	private Torrent getStubTorrent() {
		Torrent torrent = new Torrent();
		torrent.setPublicationDate(new Date());
		torrent.setModificationDate(new Date());
		torrent.setUrl("some uri");
		torrent.setTitle("some title");

		return torrent;
	}

}
