package com.suggestions.metadata.fetcher.repository;

import com.suggestions.metadata.fetcher.MetadataFetcherApplication;
import com.suggestions.metadata.fetcher.domain.Torrent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MetadataFetcherApplication.class)
public class TorrentRepositoryTest {

	@Autowired
	private TorrentRepository repository;

    @Test
    public void shouldFindByTitle() {

        Torrent stub = getStubTorrent();
        repository.save(stub);

        Torrent found = repository.findByTitle(stub.getTitle());
        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getPublicationDate(), found.getPublicationDate());
        Assert.assertEquals(stub.getModificationDate(), found.getModificationDate());
        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getTitle(), found.getTitle());
    }


    @Test
    public void shouldSave() {
        Torrent stub = getStubTorrent();
        repository.save(stub);

        Torrent found = repository.findByTitle(stub.getTitle());

        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getPublicationDate(), found.getPublicationDate());
        Assert.assertEquals(stub.getModificationDate(), found.getModificationDate());
        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getTitle(), found.getTitle());

        Assert.assertNull(found.getRating());
        Assert.assertNull(found.getGenres());

        stub.setRating(4.5);
        List<String> genres = new ArrayList<>(3);
        genres.add("genre1");
        genres.add("genre2");
        genres.add("genre3");
        stub.setGenres(genres);

        repository.save(stub);

        found = repository.findByTitle(stub.getTitle());
        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getPublicationDate(), found.getPublicationDate());
        Assert.assertEquals(stub.getModificationDate(), found.getModificationDate());
        Assert.assertEquals(stub.getUrl(), found.getUrl());
        Assert.assertEquals(stub.getTitle(), found.getTitle());

        Assert.assertEquals(stub.getRating(), found.getRating());
        Assert.assertEquals(stub.getGenres(), found.getGenres());
    }


    private Torrent getStubTorrent() {
        Torrent torrent = new Torrent();
        torrent.setPublicationDate(new Date());
        torrent.setModificationDate(new Date());
        torrent.setUrl("some uri");
        torrent.setTitle("some title");

        return torrent;
    }

}
