package com.suggestions.metadata.fetcher.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.suggestions.metadata.fetcher.MetadataFetcherApplication;
import com.suggestions.metadata.fetcher.domain.Torrent;
import com.suggestions.metadata.fetcher.service.MetadataFetcherService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MetadataFetcherApplication.class)
@WebAppConfiguration
public class MetadataFetcherControllerTest {

	private static final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private MetadataFetcherController metadataFetcherController;

	@Mock
	private MetadataFetcherService metadataFetcherService;

//	@Mock
//	private AccountService accountService;
//
	private MockMvc mockMvc;

	@Before
	public void setup() {
		initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(metadataFetcherController).build();
	}


	@Test
	public void shouldFetchAndSave() throws Exception {
		Torrent torrent = new Torrent();
		torrent.setTitle("some title");
		torrent.setUrl("url req");

		Torrent resultTorrent = new Torrent();
		resultTorrent.setTitle(torrent.getTitle());
		resultTorrent.setUrl("url resp");

		Mockito.when(metadataFetcherService.fetchAndSave(Matchers.any(Torrent.class))).thenReturn(resultTorrent);

		String json = mapper.writeValueAsString(torrent);
		System.out.println(json);

		mockMvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).content(json))
				.andExpect(jsonPath("$.title").value(resultTorrent.getTitle()))
//				.andExpect(jsonPath("$.url").value(resultTorrent.getUrl()))
				.andExpect(status().isOk())
				.andDo(result -> System.out.println(result.getResponse().getContentAsString()));
	}

}
