package com.suggestions;

import com.suggestions.metadata.fetcher.MetadataFetcherApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MetadataFetcherApplication.class)
@WebAppConfiguration
public class MetadataFetcherServiceApplicationTests {

	@Test
	public void contextLoads() {

	}

}
