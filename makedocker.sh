#!/bin/bash

#infrastructure
docker build -t blackjedi/dockerepo:suggestions-config      ./config
docker build -t blackjedi/dockerepo:suggestions-registry    ./registry
docker build -t blackjedi/dockerepo:suggestions-monitoring  ./monitoring
docker build -t blackjedi/dockerepo:suggestions-gateway     ./gateway
docker build -t blackjedi/dockerepo:suggestions-mongodb     ./mongodb

#app logic
docker build -t blackjedi/dockerepo:suggestions-account-service             ./account-service
docker build -t blackjedi/dockerepo:suggestions-suggestion-service          ./suggestion-service
docker build -t blackjedi/dockerepo:suggestions-rss-parser-service          ./rss-parser-service
docker build -t blackjedi/dockerepo:suggestions-metadata-fetcher-service    ./metadata-fetcher-service
