package com.suggestions.account.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Document(collection = "accounts")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account {

	@Id
	private String profileId;

	private Double minimalRating;

	private List<String> genresWhiteList;

	private List<String> genresBlackList;

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Double getMinimalRating() {
		return minimalRating;
	}

	public void setMinimalRating(Double minimalRating) {
		this.minimalRating = minimalRating;
	}

	public List<String> getGenresWhiteList() {
		return genresWhiteList;
	}

	public void setGenresWhiteList(List<String> genresWhiteList) {
		this.genresWhiteList = genresWhiteList;
	}

	public List<String> getGenresBlackList() {
		return genresBlackList;
	}

	public void setGenresBlackList(List<String> genresBlackList) {
		this.genresBlackList = genresBlackList;
	}
}
