package com.suggestions.account.controller;

import com.suggestions.account.domain.Account;
import com.suggestions.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

//	@PreAuthorize("#oauth2.hasScope('server') or #name.equals('demo')")
	@RequestMapping(path = "/{profileId}", method = RequestMethod.GET)
	public Account getAccountByName(@PathVariable String profileId) {
		return accountService.findById(profileId);
	}

//	@RequestMapping(path = "/current", method = RequestMethod.GET)
//	public Account getCurrentAccount(Principal principal) {
//		return accountService.findByName(principal.getName());
//	}
//
//	@RequestMapping(path = "/current", method = RequestMethod.PUT)
//	public void saveCurrentAccount(Principal principal, @Valid @RequestBody Account account) {
//		accountService.saveChanges(principal.getName(), account);
//	}

}
