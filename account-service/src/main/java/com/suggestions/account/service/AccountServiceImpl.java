package com.suggestions.account.service;

import com.suggestions.account.domain.Account;
import com.suggestions.account.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class AccountServiceImpl implements AccountService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private AccountRepository repository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Account findById(String profileId) {
		Assert.hasLength(profileId);
		log.info("searching for profile {}", profileId);
		return repository.findByProfileId(profileId);
	}
}
