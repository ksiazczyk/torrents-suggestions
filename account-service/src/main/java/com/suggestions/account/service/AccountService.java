package com.suggestions.account.service;

import com.suggestions.account.domain.Account;

public interface AccountService {

	/**
	 * Finds account by given profile ID
	 *
	 * @param profileId
	 * @return found account
	 */
	Account findById(String profileId);

}
