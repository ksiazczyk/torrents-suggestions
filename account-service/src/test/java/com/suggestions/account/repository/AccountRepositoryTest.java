package com.suggestions.account.repository;

import com.suggestions.account.AccountApplication;
import com.suggestions.account.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountApplication.class)
public class AccountRepositoryTest {

	@Autowired
	private AccountRepository repository;

	@Test
	public void shouldFindAccountByName() {

		Account stub = getStubAccount();
		repository.save(stub);

		Account found = repository.findByProfileId(stub.getProfileId());
		assertEquals(stub.getMinimalRating(), found.getMinimalRating());
		assertEquals(stub.getGenresBlackList(), found.getGenresBlackList());
		assertEquals(stub.getGenresWhiteList(), found.getGenresWhiteList());
	}

	private Account getStubAccount() {

		Account account = new Account();
		account.setProfileId("test");
		account.setMinimalRating(2.6);
		List<String> blackList = new ArrayList<>();
		blackList.add("genre1");
		blackList.add("genre2");
		blackList.add("genre3");
		account.setGenresBlackList(blackList);
		List<String> whiteList = new ArrayList<>();
		whiteList.add("white genre 1");
		whiteList.add("white genre 2");
		whiteList.add("white genre 3");
		whiteList.add("white genre 4");
		whiteList.add("white genre 5");
		account.setGenresWhiteList(whiteList);

		return account;
	}
}
