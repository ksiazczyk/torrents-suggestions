package com.suggestions.account.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.suggestions.account.AccountApplication;
import com.suggestions.account.domain.Account;
import com.suggestions.account.service.AccountService;
import com.sun.security.auth.UserPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountApplication.class)
@WebAppConfiguration
public class AccountControllerTest {

	private static final ObjectMapper mapper = new ObjectMapper();

	@InjectMocks
	private AccountController accountController;

	@Mock
	private AccountService accountService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
	}

	@Test
	public void shouldGetAccountById() throws Exception {

		final Account account = new Account();
		account.setProfileId("test");

		when(accountService.findById(account.getProfileId())).thenReturn(account);

		mockMvc.perform(get("/" + account.getProfileId()))
				.andExpect(jsonPath("$.profileId").value(account.getProfileId()))
				.andExpect(status().isOk());
	}

	@Test
	public void shouldReturnError() throws Exception {
		final Account account = new Account();
		account.setProfileId("test");

		when(accountService.findById(account.getProfileId())).thenReturn(account);

		mockMvc.perform(put("/non-existing-account")).andExpect(status().is4xxClientError());

	}

}
